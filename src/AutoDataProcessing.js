import fs from "fs";
import utils from "./Utils.js";
import autoData from "./autoDataGenerator.js";
import autoFilters from "./AutoFilters.js";

/**
 * Ejecuta un programa para generar, filtrar y gestionar información de autos.
 *
 * @param {Object} argv - Objeto que contiene las opciones y parámetros del programa.
 * @throws {Error} Si ocurre un error durante la ejecución del programa.
 */
export async function ejecutarPrograma(argv) {
    utils.verificarDirectorioYArchivo();
    let autos;

    // Comprobación de si se debe generar autos aleatoriamente o cargar desde un archivo.
    const carNum = parseInt(argv.carNum);
    if (!isNaN(carNum)) {
      console.log("\x1b[32m GENERANDO AUTOS ALEATORIOS \x1b[0m");
      autos = autoData.generarAutos(carNum); // Generar autos aleatorios.
    } else {
      // Carga autos desde un archivo JSON existente.
      const jsonAutos = fs.readFileSync("./data/autos.json", "utf-8");
      autos = JSON.parse(jsonAutos);
    }

    // Filtra autos por precio si se proporciona el parámetro "precio".
    if (argv.price) {
      autos = autoFilters.filtrarAutosPrecio({
        precio: argv.price,
        autos,
        agente: argv.agente,
      });
    }

    // Filtra autos por tipo si se proporciona el parámetro "tipo".
    if (argv.type) {
      setTimeout(function () {
        autos = autoFilters.filtrarAutos({
          atributo: "tipo",
          valor: argv.type,
          autos: autos,
          mensaje: "\x1b[33m FILTRADOS POR TIPO \x1b[0m",
          agente: argv.agente,
        });
      }, 2000);
    }

    // Filtra autos por color si se proporciona el parámetro "color".
    if (argv.color) {
      setTimeout(function () {
        autos = autoFilters.filtrarAutos({
          atributo: "color",
          valor: argv.color,
          autos: autos,
          mensaje: "\x1b[33m FILTRADOS POR COLOR \x1b[0m",
          agente: argv.agente,
        });
      }, 4000);
    }
    
    // Aumenta la popularidad de autos si se proporciona el parámetro "seleccion".
    const seleccion = parseInt(argv.seleccion);
    if (!isNaN(seleccion)) {
      setTimeout(function () {
        autoData.aumentarPopularidad(seleccion);
      }, 6000);
    }

    // Genera un archivo JSON con los autos filtrados si se aplicaron filtros.
    if (argv.type || argv.color || argv.price) {
      setTimeout(function () {
        utils.generarJSON(autos, "resultadosFiltrados.json");
      }, 8000);
    }
}
