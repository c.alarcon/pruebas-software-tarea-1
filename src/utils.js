import fs from "fs";
const utils = {
  /**
   * Genera un archivo JSON a partir de un arreglo y lo guarda en disco.
   *
   * @param {Array} arreglo - El arreglo de datos que se convertirá en JSON.
   * @param {string} nombreArchivo - El nombre del archivo JSON de salida.
   * @throws {Error} Si ocurre un error al escribir el archivo JSON.
   */
  generarJSON(arreglo, nombreArchivo) {
    /**
     * Convierte el arreglo en una cadena JSON con formato legible.
     * @type {string}
     */
    const jsonString = JSON.stringify(arreglo, null, 2);
    try {
      const ruta = "./data/" + nombreArchivo;
      fs.writeFileSync(ruta, jsonString, "utf-8");
      console.log(`Los datos se han guardado en ${ruta}`);
    } catch (error) {
      console.error("Error al escribir el archivo JSON:", error);
    }
  },
  /**
   * Imprime una lista de objetos en la consola, mostrando sus propiedades.
   *
   * @param {Array} lista - La lista de objetos que se imprimirá.
   * @param {boolean} agente - Un valor booleano que indica si se deben imprimir todas las propiedades, incluida "popularidad" (true) o si se debe omitir "popularidad" en la impresión (false).
   */
  leerLista(lista, agente) {
    if (agente) {
      for (const item of lista) {
        console.log("Auto:");
        Object.keys(item).forEach((propiedad) => {
          console.log(`${propiedad}: ${item[propiedad]}`);
        });

        console.log("-------------------------");
      }
    } else {
      for (const item of lista) {
        console.log("Auto:");
        Object.keys(item).forEach((propiedad) => {
          if (propiedad !== "popularidad") {
            console.log(`${propiedad}: ${item[propiedad]}`);
          }
        });

        console.log("-------------------------");
      }
    }
  },
  /**
   * Verifica si existe una carpeta "data" dentro del directorio, si no existen, crea la carpeta.
   *
   */
  verificarDirectorioYArchivo() {
    const directorioData = "./data";

    // Verificar si la carpeta "data" existe, si no existe, crea la carpeta.
    if (!fs.existsSync(directorioData)) {
      fs.mkdirSync(directorioData);
      console.log('Carpeta "data" creada.');
    }
  },
};

export default utils;
