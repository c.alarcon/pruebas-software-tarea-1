import utils from "./Utils.js";

const autoFilters = {
  /**
   * Filtra una lista de autos por precio máximo y muestra un mensaje en la consola.
   *
   * @param {Object} opciones - Un objeto que contiene las opciones de filtrado y mensaje.
   * @param {number} opciones.precio - El precio máximo que se utilizará para filtrar los autos.
   * @param {Array} opciones.autos - La lista de autos que se va a filtrar por precio.
   * @param {boolean} opciones.agente - Un valor booleano que indica si se deben imprimir todas las propiedades, incluida "popularidad" (true) o si se debe omitir "popularidad" en la impresión (false).
   * @returns {Array} Una nueva lista de autos que cumplen con el criterio de precio.
   */
  filtrarAutosPrecio({ precio, autos, agente }) {
    console.log("\x1b[33m FILTRADOS POR PRECIO \x1b[0m");
    const autosFiltradosPrecio = [];
    autos.forEach((auto) => {
      if (auto.precio <= precio) {
        autosFiltradosPrecio.push(auto);
      }
    });

    if (autosFiltradosPrecio.length === 0) {
      console.log("No existe autos con el precio ingresado o menor");
      process.exit(1);
    } else {
      utils.leerLista(autosFiltradosPrecio, agente);
    }
    return autosFiltradosPrecio;
  },

  /**
   * Filtra una lista de autos por un atributo y un valor específicos y muestra un mensaje en la consola.
   *
   * @param {Object} opciones - Un objeto que contiene las opciones de filtrado y mensaje.
   * @param {string} opciones.atributo - El atributo por el cual se filtrarán los autos.
   * @param {any} opciones.valor - El valor que se utilizará para filtrar los autos.
   * @param {Array} opciones.autos - La lista de autos que se va a filtrar.
   * @param {string} opciones.mensaje - El mensaje que se mostrará en la consola antes de realizar el filtrado.
   * @param {boolean} opciones.agente - Un valor booleano que indica si se deben imprimir todas las propiedades, incluida "popularidad" (true) o si se debe omitir "popularidad" en la impresión (false).
   * @returns {Array} Una nueva lista de autos que cumplen con el criterio de filtrado.
   */
  filtrarAutos({ atributo, valor, autos, mensaje, agente }) {
    console.log(mensaje);
    const autosFiltrados = [];
    autos.forEach((auto) => {
      if (auto[atributo] === valor) {
        autosFiltrados.push(auto);
      }
    });

    if (autosFiltrados.length === 0) {
      console.log("No existe auto con los parametros ingresados");
      process.exit(1);
    } else {
      utils.leerLista(autosFiltrados, agente);
    }

    return autosFiltrados;
  }
}

export default autoFilters;