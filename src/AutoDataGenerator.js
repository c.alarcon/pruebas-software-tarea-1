import fs from "fs";
import utils from "./Utils.js";
/**
 * Almacena la lista de autos generados.
 * @type {Array}
 */
const autos = [];
const marcas = ["Toyota", "Chevrolet", "Nissan", "Mazda", "Mitsubishi"];
const colores = ["Negro", "Rojo", "Azul", "Verde", "Amarillo"];
const tipo = ["Sedan", "Camioneta", "SUV"];

// Sedan 0-2 / Camioneta 3-5 / SUV 6-8
const motor = [
  "1.4cc",
  "1.6cc",
  "2.0cc",
  "2.4cc",
  "3.0cc",
  "4.0cc",
  "1.8cc",
  "2.2cc",
  "2.8cc",
];

const autoData = {
  /**
   * Genera y almacena una lista de autos aleatorios en un archivo JSON.
   *
   * @param {number} carNum - El número de autos que se generarán y almacenarán.
   * @returns {Array} Una lista de autos generados aleatoriamente.
   */
  generarAutos(carNum) {
    for (let index = 1; index < carNum+1; index++) {
      /**
       * Objeto que representa un auto con propiedades inicializadas en valores predeterminados.
       * @type {Object}
       */
      const auto = {
        id: null,
        marca: "",
        annio: null,
        color: "",
        precio: null,
        turbo: false,
        tipo: "",
        motor: "",
        cabinas: null,
        sunroof: false,
        popularidad: 0,
      };

      const valoresRandom = this.generarRandomsAuto();

      auto.id = index;
      auto.marca = valoresRandom.marca;
      auto.annio = valoresRandom.annio;
      auto.color = valoresRandom.color;
      auto.precio = valoresRandom.precio;
      auto.tipo = valoresRandom.tipo;
      auto.motor = valoresRandom.motor;
      auto.turbo = valoresRandom.turbo;
      auto.cabinas = valoresRandom.cabinas;
      auto.sunroof = valoresRandom.sunroof;

      autos.push(auto);
    }
    utils.generarJSON(autos, "autos.json");
    return autos;
  },
  /**
   * Genera un objeto con datos aleatorios para un auto.
   *
   * @returns {Object} Un objeto que contiene datos aleatorios del auto.
   */
  generarRandomsAuto() {
    let marcaRandom = Math.floor(Math.random() * 5);
    const marcaRandomString = marcas[marcaRandom];
    const annioRandom = Math.floor(Math.random() * 9) + 2015;
    const colorRandom = Math.floor(Math.random() * 5);
    const colorRandomString = colores[colorRandom];
    const precioRandom = Math.floor(Math.random() * 22000001) + 8000000;
    const tipoRandom = Math.floor(Math.random() * 3);
    const tipoRandomString = tipo[tipoRandom];
    const booleanoRandom = Math.random() < 0.5;
    let cabinasRandom = null;
    let sunroofRandom = null;
    let motorRandom = null;
    if (tipoRandomString == "Sedan") {
      motorRandom = Math.floor(Math.random() * 3);
    } else if (tipoRandomString == "Camioneta") {
      motorRandom = Math.floor(Math.random() * 3) + 3;
      cabinasRandom = Math.floor(Math.random() * 2) + 1;
    } else if (tipoRandomString == "SUV") {
      motorRandom = Math.floor(Math.random() * 3) + 6;
      sunroofRandom = Math.random() < 0.5;
    }
    const motorRandomString = motor[motorRandom];

    const randoms = {
      marca: marcaRandomString,
      annio: annioRandom,
      color: colorRandomString,
      precio: precioRandom,
      tipo: tipoRandomString,
      turbo: booleanoRandom,
      motor: motorRandomString,
      cabinas: cabinasRandom,
      sunroof: sunroofRandom,
    };
    return randoms;
  },/**
  * Aumenta la popularidad de un auto en la lista de autos por su ID.
  *
  * @param {number} id - El ID del auto cuya popularidad se debe aumentar.
  */
 aumentarPopularidad(id) {
   const jsonAutos = fs.readFileSync("./data/autos.json", "utf-8");
   const autos = JSON.parse(jsonAutos);
   const auto = autos.find((auto) => auto.id === id);
   if (auto) {
     const popularidad = auto.popularidad + 1;
     const cambios = {
       popularidad: popularidad,
     };
     Object.assign(auto, cambios);

     fs.writeFileSync(
       "./data/autos.json",
       JSON.stringify(autos, null, 2),
       "utf-8"
     );
     console.log("Popularidad Aumentada:", auto);
   } else {
     console.log("-------------------------");
     console.log(
       "\x1b[31m%s\x1b[0m",
       "El ID entregado no coincide con ningún auto registrado."
     );
   }
 }
};
export default autoData;
