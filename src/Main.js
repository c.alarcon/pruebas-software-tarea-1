import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import { ejecutarPrograma } from "./AutoDataProcessing.js";

const argv = yargs(hideBin(process.argv))
  .option("carNum", {
    describe: "Número de autos a generar",
    type: "number",
  })
  .option("agente", {
    describe: "Habilitar modo agente",
    type: "boolean",
  })
  .option("price", {
    describe: "Filtrar por precio",
    type: "number",
  })
  .option("type", {
    describe: "Filtrar por tipo de auto",
    type: "string",
  })
  .option("color", {
    describe: "Filtrar por color de auto",
    type: "string",
  })
  .option("seleccion", {
    describe: "Aumentar la popularidad de un auto",
    type: "string",
  }).argv;

ejecutarPrograma(argv); // Llama a la función que contiene la lógica del software
