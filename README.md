# Tarea 1 - Pruebas de software

Integrantes: Christopher Alarcon - César Soto

Link de repositorio: https://gitlab.com/c.alarcon/pruebas-software-tarea-1

### Install libraries

`npm install`

### OPTIONS

### Create a json with n cars

`npm run start -- --carNum=NumeroDeAutos`

### Filter the cars by price (<=) (numbers between 8000000 and 30000000)

`npm run start -- --price=price`

### Filter the cars by type (Camioneta, SUV, Sedan)

`npm run start -- --type=type`

### Filter the cars by color (Rojo, Azul, Verde, Amarillo, Negro)

`npm run start -- --color=color`

### Filter the cars by price, type and color (options order doesn`t matter)

`npm run start -- --price=price --type=type --color=color`

### Access as an Agent (it will show the popularity of the cars)

`npm run start -- (any filter) --agente=true`

### Add Popularity to a Car

`npm run start -- --seleccion=IdOfTheCar`

# Archivo AutoDataGenerator.js

Genera datos aleatorios de autos, los almacena en un archivo JSON y proporciona la funcion de aumentar la popularidad de un auto específico en la lista.

# Archivo AutoDataProcessing.js

Se encarga de procesar los datos existentes de los autos para hacer uso de opciones de filtrados en estos, con ello, los autos que se ajusten a los filtros, se almacenaran en un archivo Json.

# Archivo AutoFilters.js

Contiene un conjunto de funciones relacionadas con la filtración de una lista de autos en función de diferentes criterios, tales como: precio, tipo, color, etc.